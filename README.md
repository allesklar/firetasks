# Firetasks

Writing mid-to-large sized applications on Firebase can be too complex. Data replication, cascading processes, event delivery, or other multiple-branch actions are difficult to build for clients and servers.

**Firetasks** is a library for building resilent Firebase task queues using Typescript. Tasks are actionable and completable events abstracted over Firebase JSON trees. Firetasks makes it easy to manage tasks on queues in your realtime database.

## License

Firetasks is Open Source and available under the Apache 2 License.
