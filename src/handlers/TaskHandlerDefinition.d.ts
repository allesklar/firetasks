/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */
import Task from '../core/Task';
export default interface TaskHandlerDefinition {
    [taskDomain: string]: {
        [taskAction: string]: {
            taskType: typeof Task;
            handler: (Task, Event) => Promise<void>;
        };
    };
}
