/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */

import * as functions from 'firebase-functions';
import Event = functions.Event
import DeltaSnapshot = functions.database.DeltaSnapshot;

import { JsObject } from 'ts-json-definition';

import { computeHashCode, SERVER_VALUE } from '../common';
import Task from '../core/Task';
import TaskError from '../core/TaskError';
import TaskHandlerDefinition from '../handlers/TaskHandlerDefinition';

export default class TaskQueue {

    static fromFirebaseEvent<T extends Task>(event: Event<DeltaSnapshot>, taskDefs: TaskHandlerDefinition): Promise<void> {
        if (!event.data.exists()) return Promise.resolve();

        const taskJson = event.data.val();

        if (taskJson.hasOwnProperty('_code')) {
            return Promise.resolve(); // do nothing
        }

        console.log('Task received', taskJson);
        if (!taskJson._domain) {
            return TaskQueue.handleMissingField(event, taskJson, '_domain');
        }
        else if (taskJson._domain && !Object.keys(taskDefs).includes(taskJson._domain)) {
            return TaskQueue.handleUnsupportedDomain(event, taskJson);
        }

        const domain = taskDefs[taskJson._domain];
        if (!taskJson._action) {
            return TaskQueue.handleMissingField(event, taskJson, '_action');
        }
        else if (taskJson._action && !Object.keys(domain).includes(taskJson._action)) {
            return TaskQueue.handleUnsupportedAction(event, taskJson);
        }

        const action = domain[taskJson._action];

        const runFunctionPromise: Promise<void> = new Promise((resolve, reject) => {
            action.taskType.fromJsObject<T>(taskJson).match<void>({
                Left: err => {
                    resolve(TaskQueue.handleError(event, taskJson, err))
                },
                Right: task => {
                    task._ref = event.data.ref;
                    const errors = task.validate();
                    if (errors.nonEmpty) {
                        resolve(TaskQueue.handleError(event, taskJson, errors.get()));
                    }
                    else {
                        try {
                            action.handler(task, event)
                                .then(() => { resolve(); }) // Workaround for Firebase SDK timeout issue
                                // See https://stackoverflow.com/questions/43151022/cloud-functions-for-firebase-onwrite-timeout
                                .catch(err => resolve(TaskQueue.handleError(event, taskJson, err)));
                        }
                        catch (err) {
                            resolve(TaskQueue.handleError(event, taskJson, err));
                        }
                    }
                }
            })
        });

        return runFunctionPromise;
    }

    private static handleError(event: Event<DeltaSnapshot>, taskJson: JsObject, err: any): Promise<void> {
        let payload = {};

        if (err instanceof TaskError) {
            payload = Object.assign(
                {
                    _code: err.code,
                    _message: err.message,
                    _devMessage: err.causesAsString(),
                    _extendedCode: err.extendedCode.nonEmpty ? err.extendedCode.get() : null,
                    _state: 'error',
                    _stateChanged: SERVER_VALUE
                },
                taskJson);
        }
        else {
            const hashCode = computeHashCode(JSON.stringify(err));
            console.error('Error', hashCode, 'log', err);
            console.error('Error', hashCode, 'payload', taskJson);
            payload = Object.assign(
                {
                    _code: 500,
                    _message: 'Couldn\'t successfully complete your request',
                    _devMessage: 'Internal server error - please contact system administrator with the contents of this message.',
                    _state: 'error',
                    _stateChanged: SERVER_VALUE,
                    _referenceCode: hashCode
                },
                taskJson);
        }
        return event.data.ref.update(payload);
    }

    private static handleUnsupportedDomain(event: Event<DeltaSnapshot>, taskJson: JsObject): Promise<void> {
        const payload = Object.assign(
            {
                _code: 404,
                _message: 'Your request is invalid',
                _devMessage: 'Domain not found for this queue. Please check documentation for supported domains.',
                _state: 'error',
                _stateChanged: SERVER_VALUE
            },
            taskJson);

        return event.data.ref.update(payload);
    }

    private static handleUnsupportedAction(event: Event<DeltaSnapshot>, taskJson: JsObject): Promise<void> {
        const payload = Object.assign(
            {
                _code: 405,
                _message: 'Your request is invalid',
                _devMessage: 'Method not allowed. This action is not supported for this domain. Please check documentation for supported actions.',
                _state: 'error',
                _stateChanged: SERVER_VALUE
            },
            taskJson);

        return event.data.ref.update(payload);
    }

    private static handleMissingField(event: Event<DeltaSnapshot>, taskJson: JsObject, field: string): Promise<void> {
        const payload = Object.assign(
            {
                _code: 422,
                _message: 'Your request is invalid',
                _devMessage: `Missing field: ${field}`,
                _state: 'error',
                _stateChanged: SERVER_VALUE
            },
            taskJson);

        return event.data.ref.update(payload);
    }

}
