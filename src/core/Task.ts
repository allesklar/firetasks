/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */

import * as _ from 'lodash';
import * as admin from 'firebase-admin';
import Reference = admin.database.Reference;

import { Serializable, Serialize, SerializeOpt } from 'ts-serialize';
import { Optional, Some, None } from 'scalts';
import { JsObject } from 'ts-json-definition';

import TaskError from './TaskError';
import { deleteProperties, SERVER_VALUE } from '../common';

export default abstract class Task extends Serializable {
    @Serialize() _domain: string;
    @Serialize() _action: string;
    @SerializeOpt(String) _location: Optional<string>;
    @SerializeOpt(String) _state: Optional<string>;
    @SerializeOpt(Number) _code: Optional<number>;
    @SerializeOpt(Number) _extendedCode: Optional<number>;
    @SerializeOpt(String) _message: Optional<string>;
    @SerializeOpt(String) _devMessage: Optional<string>;
    _ref: Reference;

    success(msg: Optional<string> = None,
        devMsg: Optional<string> = None,
        extCode: Optional<number> = None): Task {
        return this.successWithCode(200, msg, devMsg, extCode);
    }

    successWithCode(code: number,
        msg: Optional<string> = None,
        devMsg: Optional<string> = None,
        extCode: Optional<number> = None): Task {
        const newTask = _.cloneDeep(this);
        newTask._state = Some('success');
        newTask._code = Some(code);
        newTask._extendedCode = extCode;
        newTask._message = msg;
        newTask._devMessage = devMsg;
        return newTask;
    }

    error(code: number,
        msg: Optional<string> = None,
        devMsg: Optional<string> = None,
        extCode: Optional<number> = None): Task {
        const newTask = _.cloneDeep(this);
        newTask._state = Some('error');
        newTask._code = Some(code);
        newTask._extendedCode = extCode;
        newTask._message = msg;
        newTask._devMessage = devMsg;
        return newTask;
    }

    abstract validate(): Optional<TaskError>;

    sanitize(remove: string[] = [], keep: string[] = []): Object {
        return deleteProperties(this.toJson(), (o, k) => {
            return !keep.includes(k) && (k.startsWith('_') || remove.includes(k))
        });
    }

    toJson(): JsObject {
        const obj = _.assign({ "_stateChanged": SERVER_VALUE }, super.toJson());
        return deleteProperties(obj, (o, k) => { return o[k] === null });
    }

}
