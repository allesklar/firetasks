/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */

import { Optional, None } from 'scalts';

export default class TaskError implements Error {
    public name: string = 'TaskError';

    constructor(
        readonly code: number,
        readonly message: string,
        readonly devMessages: string[] = [],
        readonly extendedCode: Optional<number> = None) { }

    causesAsString(): string | null {
        if (this.devMessages.isEmpty) return null;
        else if (this.devMessages.length === 1) return this.devMessages[0];
        else return this.devMessages.map(s => `"${s}"`).join(',');
    }
}

